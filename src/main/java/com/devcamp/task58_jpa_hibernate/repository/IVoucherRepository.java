package com.devcamp.task58_jpa_hibernate.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task58_jpa_hibernate.models.CVoucher;


public interface IVoucherRepository extends JpaRepository<CVoucher, Long> {
    
}
