package com.devcamp.task58_jpa_hibernate.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task58_jpa_hibernate.models.CVoucher;
import com.devcamp.task58_jpa_hibernate.repository.IVoucherRepository;

import java.util.*;

@CrossOrigin
@RestController
public class CVoucherController {
    @Autowired
    IVoucherRepository voucherResponitory;

    @GetMapping("/vouchers")
    public ResponseEntity<ArrayList<CVoucher>> getVouchers() {
        try {
            ArrayList<CVoucher> listVoucher = new ArrayList<CVoucher>();
            voucherResponitory.findAll().forEach(listVoucher::add);
            return ResponseEntity.ok(listVoucher);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
